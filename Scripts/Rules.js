




// FUNCIONES DE RULES Y MENU DINAMICO

function getAudiosMenu(rulesResponse)
{

return getElement("Telecom","audios",rulesResponse);


}




function getUrlSubrutina(rulesResponse)
{

var subrutina =getElement("Telecom","subrutina",rulesResponse) ; 
var VXML = 'Main';
    
return '../../' + subrutina + '/src-gen/' + VXML + '.vxml';

}


function getSubrutina(rulesResponse)
{


var subrutina =getElement("Telecom","subrutina",rulesResponse) ; 

return subrutina; 

}

function opcionCorrecta(rulesResponse)
{
var subrutina =getElement("Telecom","subrutina",rulesResponse) ;
	
	if (subrutina != undefined && subrutina != "INVALID" )
	{
	return true;	
	}
	else
	{
	return false;	
	}
}
/// 
function getMenuNext(rulesResponse)
{
	var menu ='';
	menu =getElement("Telecom","menuNext",rulesResponse);;
	
	return menu;	
		
}

function getGIMKey(rulesResponse)
{
	var menu ='';
	menu =getElement("Telecom","gimkey",rulesResponse);;
	
	return menu;	
	
	
}







function getDerivacion(rulesResponse)
{
	
var derivacion ='';
	
derivacion=	getElement("Telecom","derivacion",rulesResponse);
	

return derivacion;
}
function getAudios(rulesResponse)
{
	
var derivacionAudio ='';
	
derivacionAudio=	getElement("Telecom","audios",rulesResponse);
	

return derivacionAudio;
}


function getDN(rulesResponse)
{
	var dn ='';
	dn=	getElement("Telecom","dn",rulesResponse);;
	
	return dn;	
	
	
}

function getElement(clase,tag,rulesResponse)
{

	
if (rulesResponse != undefined)
{	
	if (rulesResponse["knowledgebase-response"] != undefined)
	{	
		if (rulesResponse["knowledgebase-response"]["inOutFacts"] != undefined)
		{	
		
var nFact =	rulesResponse["knowledgebase-response"]["inOutFacts"]["named-fact"];

// PACKAGE DINAMICO
var package = rulesResponse["knowledgebase-response"]["inOutFacts"]["named-fact"][0]["fact"]["@class"];
var vclase = package.replace("_GRS_Environment","")+ clase;
	
for (var i=0 ;i < nFact.length ;i++){
	
	if (nFact[i]["fact"]["@class"]== vclase)
	{
		return  nFact[i]["fact"][tag];
	}
}
}
}
}
return undefined;

}	

function getRuleApplied(rulesResponse)
{
if (rulesResponse != undefined)
{	
	if (rulesResponse["knowledgebase-response"] != undefined)
	{	
		if (rulesResponse["knowledgebase-response"]["executionResult"] != undefined)
		{	
		 if (rulesResponse["knowledgebase-response"]["executionResult"]["rulesApplied"] != undefined && rulesResponse["knowledgebase-response"]["executionResult"]["rulesApplied"]!= '')
		   {	
			var nFact =	rulesResponse["knowledgebase-response"]["executionResult"]["rulesApplied"]["string"];
	        var rule;
	   	      for (var i=0 ;i < nFact.length ;i++){
	
			   rule =  nFact[i];
			     }
			    return rule;
		    }
         }
      }
}
return undefined;


}


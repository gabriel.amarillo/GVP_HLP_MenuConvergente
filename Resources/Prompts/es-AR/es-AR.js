var version = "8.1.41";

/* 
 * PlayBuiltInType.js
 *
 * JavaScript functions for playing dynamic prompts in VoiceXML on
 * the Telera Platform
 *
 * Author: Scott Atwood <scott.atwood@telera.com>
 * Date: $Date: 3/11/03 2:39p $
 * Copyright � 2002 Telera Inc.
 */

/*
 * Problem comments:
 *
 * What are the names and organizational structure of the vox files
 * which I can refer to?
 *
 * Is this standardized across locales, or each each new locale author
 * supposed to come up with one himself or herself?
 *
 * I need at least the following:
 *   days of the week
 *   months of the year
 *   cardinals 0-99, 100, 200, 300, 400, 500, 600, 700, 800, 900, thousand,
 *      million, billion, trillion
 *   ordinals 1st-99th, 100th, 200th, 300th, 400th, 500th, 600th, 700th,
 *      800th, 900th, thousandth, millionth, billionth, trillionth
 *   "true", "false"
 *   "dollar", "dollars", "euro", "euros", "Swiss franc", "Swiss francs",
 *       "pound", "pounds", "yen", etc.
 *   "cent", "cents", "centime", "centimes", "pence", "centavo",
 *       "centavos", "sen", etc.
 *   "the" pronounced D@ (thuh)
 *   "o'clock"
 *   500ms silence
 *   "and"
 *
 * How do I know whether I should speak the day of the week or not?
 */
 
/**
 * The base URL that is prepended to the URLs for all vox files.
 */
 
var promptBaseUrl = "../Resources/Prompts/es-AR/";

// var promptBaseUrl = "http://10.10.10.234/vxml/indexfile/VoxFiles/es-CO/";

/**
 * Default audio file extension
 */

var audioFileExtension = ".vox";

/**
 * Dummy class to hold constants
 */

function Format() {}
new Format();

/*
 * These flags may be bitwise OR'd together to specfy the date output format
 */
/**
 * Flag to speak the month
 */ 

Format.prototype.SPEAK_MONTH = 1;

/**
 * Flag to speak the day
 */

Format.prototype.SPEAK_DAY = 2;

/**
 * Flag to speak both month and day.  This is a shortcut for
 * f.SPEAK_MONTH | f.speak_DAY
 */

Format.prototype.SPEAK_MONTH_AND_DAY = 3;

/**
 * Flag to speak the year
 */

Format.prototype.SPEAK_YEAR = 4;

/**
 * Flag to speak the day of the week
 */

Format.prototype.SPEAK_DAY_OF_WEEK = 8;

/**
 * Translates the value of the specified type into an array of
 * vox file URLs.  The array is a list of vox files that when
 * played in succession will represent the value of the specified
 * type in a locale-specific manner.  The following types are
 * supported:
 *
 * <table>
 * <tr><td>"boolean"</td><td>"true" or "false"</td></tr>
 * <tr><td>"date"</td><td>YYYYMMDD<br>Unspecified fields may
 *                                   be replaced by "??" or "????"</td></tr>
 * <tr><td>"digits"</td><td>A string of 0 or more characters [0-9]</td></tr>
 * <tr><td>"currency"</td><td>UUUMM.NN<br>Where UUU is the ISO4217 currency
 *                            code.  The currency code may be omitted, in
 *                            which case, the default currency for the
 *                            current locale will be used.</td></tr>
 * <tr><td>"number"</td><td>Positive or negative, integer or decimal
 *                          number</td></tr>
 * <tr><td>"phone"</td><td>Sequence of digits [0-9], optionally followed
 *                         by an "x" and extension digits [0-9]</td></tr>
 * <tr><td>"time"</td><td>hhmm[aph?]<br>"a" means AM, "p" means PM,
 *                        h means 24 hour, and ? means ambiguous</td></tr>
 * <tr><td>"ordinal"</td><td>A positive integer</td></tr>
 * <tr><td>"alphanumeric"</td><td>A string of digits [0-9] and US-ASCII
 *                                characters [A-Za-z]</td></tr>
 * <tr><td>"dtmf"</td><td>A string of zero or more DTMF characters
 *                        [0-9A-D*#]</td></tr>
 *
 * @param value The value to translate.
 * @param type The type of the value.
 * @param format The output format [optional].
 * @return An array of URL strings.
 */

function esARPlayBuiltinType(value, type, promptUrl, format)
{
    type = type.toLowerCase();
    // Sets the new base URL that is prepended to the URLs for all audio files.
    if (!(typeof promptUrl == 'undefined'))
    	promptBaseUrl = promptUrl;
    
    var format;
    
    switch (type) {
        case 'boolean':
            return booleanPrompts(value);
        case 'date':
            return datePrompts(value, format);
        case 'digits':
            return alphanumericPrompts(value);
        case 'currency':
            return currencyPrompts(value);
        case 'number':
            return cardinalPrompts(value);
        case 'phone':
            return phonePrompts(value);
        case 'time':
            return timePrompts(value);
        case 'ordinal':
            return ordinalPrompts(value);
        case 'alphanumeric':
            return alphanumericPrompts(value);
        case 'dtmf':
            return dtmfPrompts(value);
        default:
            return void 0;
    }
}

function booleanPrompts(value)
{
    var promptsArray = new Array(1);
    if (value) {
        promptsArray[0] = promptBaseUrl + "miscellaneous/true" + audioFileExtension;
    } else {
        promptsArray[0] = promptBaseUrl + "miscellaneous/false" + audioFileExtension;
    }
    return promptsArray;
}

/**
 * Translates the date value into an array of vox file URLs
 * for playing the date.  The date must be in the ISO-8601
 * condensed format of YYYYMMDD.  One or more fields may be
 * left unspecified by substituting question mark characters
 * ("?") for a numeric value.  Output format will be in a
 * locale-specific ordering.   The format specification can
 * modify which fields are spoken.  If no format specification
 * parameter is given, the month and day are spoken.  Otherwise,
 * the date fields specified in the format specification will be
 * spoken.  Each output field is represented by a constant in the
 * Format class.  Multiple output fields are specified by 
 * bitwise OR'ing the appropriate constants.
 *
 * <pre>
 * Format f = new Format();
 * PlayBuiltinType("20020823", "date", f.SPEAK_MONTH |
 *                                     f.SPEAK_DAY |
 *                                     f.SPEAK_YEAR |
 *                                     f.SPEAK_DAY_OF_WEEK);
 * </pre>
 *
 * @param value The date in YYYYMMDD format.
 * @param format An optional format specification
 * @return An array of URL strings.
 */

function datePrompts(value, format)
{
  var year = value.substring(0, 4);
  var month = value.substring(4, 6);
  var day = value.substring(6, 8);
  var speakMonth;
  var speakDay;
  var speakYear;
  var speakDayOfWeek;
  var f = new Format();
   
  if (format != undefined) {
      speakMonth = format & f.SPEAK_MONTH;
      //speakDay = format & f.SPEAK_DAY;
      speakDay = true;
      speakYear = format & f.SPEAK_YEAR;
      speakDayOfWeek = format & f.SPEAK_DAY_OF_WEEK;
  } else {
      speakYear = true;
      speakDayOfWeek = false;
      speakDay = true;
      speakMonth = true;
  }
    
  if (day.substring(0, 1) == "0") {
      day = day.substring(1, 2);
  }
  
  var yearKnown = !(year == "????");
  var monthKnown = !(month == "??");
  var dayKnown = !(day == "??");
  
  if ((!yearKnown && !monthKnown && !dayKnown) ||
      (yearKnown && !monthKnown && dayKnown)) {
      return void 0;
  }
    
  var promptsArray;
  if (!yearKnown && !monthKnown && dayKnown) {
    promptsArray = new Array();
    /* In US English, a date phrase containing only the
      * day of the month begins with a definite article.
      */
   
    /* In US English, if the word following a definite article
     * begins with a vowel, the definite article is pronounced
     * "THEE".  If the word following the definite article 
     * begins with a consonant, the definite article is
     * pronounced "THUH".
     */
    if (speakDay) { 
    	// Solo se reproduce para lenguaje ingles
      //if (day == 8 || day == 11 || day == 18) {
      //  Solo se reproduce para lenguaje ingles
      //  promptsArray.push(promptBaseUrl + "miscellaneous/the1" + audioFileExtension);
      //} else {
      //    Solo se reproduce para lenguaje ingles
      //    promptsArray.push(promptBaseUrl + "miscellaneous/the2" + audioFileExtension);
      //}
      promptsArray = promptsArray.concat(ordinalPrompts(day));
      return promptsArray;
    } else {
        return void 0;
    }
  }
   
  if (yearKnown && !monthKnown && !dayKnown) {
    if (speakYear) {
      return yearPrompts(year);
    } else {
        return void 0;
    }
  }
  
  if (!yearKnown && monthKnown && !dayKnown) {
    if (speakMonth) {
      return monthPrompts(month);
    } else {
        return void 0;
    }
  }
    
  if (yearKnown && monthKnown && !dayKnown) {
    promptsArray = new Array();
    if (speakMonth) {
      promptsArray = promptsArray.concat(monthPrompts(month));
    }
     
    if (speakYear) {
      promptsArray = promptsArray.concat(yearPrompts(year));
    }
     
    if (promptsArray.length > 0) {
      return promptsArray;
    } else {
        return void 0;
    }
  }

  if (!yearKnown && monthKnown && dayKnown) {
    promptsArray = new Array();
    if (speakMonth) {
      promptsArray = promptsArray.concat(monthPrompts(month));
    }
   
    if (speakDay) {
       promptsArray = promptsArray.concat(ordinalPrompts(day));
    }
   
    if (promptsArray.length > 0) {
      return promptsArray;
    } else {
        return void 0;
    }
  }
    
  if (yearKnown && monthKnown && dayKnown) {
    promptsArray = new Array();
    
    if (speakDayOfWeek) {
      // JavaScript months are 0-indexed.
      var date = new Date(year, month - 1, day);
      var dayOfWeek = date.getDay();
      promptsArray = promptsArray.concat(dayOfWeekPrompts(dayOfWeek));
    }
    
    if (speakDay) {
      promptsArray = promptsArray.concat(ordinalPrompts(day));
    }
		
	  if (speakMonth && speakDay) {
      promptsArray.push(promptBaseUrl + "miscellaneous/de" + audioFileExtension);
      promptsArray = promptsArray.concat(monthPrompts(month));
    }else {  
    	 promptsArray = promptsArray.concat(monthPrompts(month));
    }
      
    if (speakYear) {
      promptsArray.push(promptBaseUrl + "miscellaneous/del" + audioFileExtension);
      promptsArray = promptsArray.concat(yearPrompts(year));
    }
      
    if (promptsArray.length > 0) {
      return promptsArray;
    } else {
        return void 0;
    }
  }
}

/**
 * Translates the currency value into an array of vox file URLs
 * for playing the currency.  The currency amount must be a number
 * which may contain either a whole number part, or a decimal part, or
 * both, and is optionally preceeded by a currency specifier.  The
 * currency specifier may either be an ISO-4217 currency code, e.g.
 * USD, EUR, JPY, CHF, CAD, GBP, or MXN, or it may be one of the
 * following currency symbols: $ = USD, &#163; = GBP, &#8364; = EUR,
 * &#165; = JPY.  If no currency is specified, the currency of the
 * current locale will be used, e.g. USD for es-ARS, and EUR for fr-FR.
 *
 * @param value The currency amount.
 * @return An array of URL strings.
 */

function currencyPrompts(value)
{
  var currency;
  var valueStart;
  var decimalPoint;
  var fraction;
  var amount;
  
  /*
  var isNegative;
  if (value.charAt(0) == "-") {
   	value = value.substring(1);
  	isNegative = true;
  }
  */
  
  if (value.charAt(0) == "$") {
    currency = "USD";
    valueStart = 1;
  } else if (value.charAt(0) == "\u00A3") {
      currency = "GBP";
      valueStart = 1;
  } else if (value.charAt(0) == "\u00A5") {
      currency = "JPY";
      valueStart = 1;
  } else if (value.charAt(0) == "\u20AC" || value.charAt(0) === "\u0080") {
      currency = "EUR";
      valueStart = 1;
  } else if (value.substring(0, 3).match(/^[A-Za-z][A-Za-z][A-Za-z]$/)) {
      currency = value.substring(0, 3);
      valueStart = 3;
  } else {
       currency = "ARS";
       valueStart = 0;
  }

  decimalPoint = value.indexOf(".");
  if (decimalPoint == -1) {
    amount = value.substring(valueStart);
    fraction = 0;
  } else if (decimalPoint == 3) {
      if (valueStart < decimalPoint) {
        amount = value.substring(valueStart, decimalPoint);
      } else {
          amount = 0;
      }
      fraction = value.substring(decimalPoint + 1);
  } else {
      amount = value.substring(valueStart, decimalPoint);
      fraction = value.substring(decimalPoint + 1);
  }

  var promptsArray = new Array();
  if (amount < 0) {
    promptsArray.push(promptBaseUrl + "miscellaneous/minus" + audioFileExtension);
    amount = amount * -1;
  }

  if (amount > 0) {
    promptsArray = promptsArray.concat(cardinalPrompts(amount));
    promptsArray = promptsArray.concat(currencyNamePrompts(currency, amount != 1));
    if (fraction > 0) {
      promptsArray.push(promptBaseUrl + "miscellaneous/con_dec" + audioFileExtension);
    }
  }

  if (fraction > 0) {
  	if (fraction.length == 1)
     	fraction += '0';
      /*/ done adding*/
      promptsArray = promptsArray.concat(cardinalPrompts(fraction));
      promptsArray = promptsArray.concat(subcurrencyNamePrompts(currency, fraction != 1));
  }
 
  if (amount == 0 && fraction == 0) {
    promptsArray = promptsArray.concat(cardinalPrompts(0));
    promptsArray = promptsArray.concat(currencyNamePrompts(currency, true));
  }
  return promptsArray;
}

function currencyNamePrompts(value, isPlural)
{
  value = value.toUpperCase();
  switch(value) {
    case "USD":
      if (isPlural) {
        return new Array(promptBaseUrl + "miscellaneous/dollars.vox");
      } else {
          return new Array(promptBaseUrl + "miscellaneous/dollar.vox");
      }
    case "GBP":
      if (isPlural) {
        return new Array(promptBaseUrl + "miscellaneous/poundssterling.vox");
      } else {
          return new Array(promptBaseUrl + "miscellaneous/poundsterling.vox");
      }
    case "EUR":
      if (isPlural) {
        return new Array(promptBaseUrl + "miscellaneous/euros.vox");
      } else {
          return new Array(promptBaseUrl + "miscellaneous/euro.vox");
      }
    case "JPY":
      return new Array(promptBaseUrl + "miscellaneous/yen.vox");
    case "DEM":
      if (isPlural) {
        return new Array(promptBaseUrl + "miscellaneous/marks.vox");
      } else {
          return new Array(promptBaseUrl + "miscellaneous/mark.vox");
      }
    case "ITL":
      if (isPlural) {
        return new Array(promptBaseUrl + "miscellaneous/lira.vox");
      } else {
          return new Array(promptBaseUrl + "miscellaneous/lire.vox");
      }
    case "FRF":
      if (isPlural) {
        return new Array(promptBaseUrl + "miscellaneous/francs.vox");
      } else {
          return new Array(promptBaseUrl + "miscellaneous/franc.vox");
      }
    case "ARS":
      if (isPlural) {
        return new Array(promptBaseUrl + "miscellaneous/pesos" + audioFileExtension);
      } else {
          return new Array(promptBaseUrl + "miscellaneous/peso" + audioFileExtension);
      }
  
      /*
      case "CAD":
        if (isPlural) {
          return new Array("Canadian dollars"); //FIXME
        } else {
            return new Array("Canadian dollar"); //FIXME
        }
      case "MXN":
        if (isPlural) {
          return new Array("pesos"); //FIXME
        } else {
            return new Array("peso"); //FIXME
        }
      case "CHF":
        if (isPlural) {
          return new Array("Swiss francs"); //FIXME
        } else {
            return new Array("Swiss franc"); //FIXME
        }
  
      */
  
      default:
        return void 0;
  }
}

function subcurrencyNamePrompts(value, isPlural)
{
    value = value.toUpperCase();
    switch(value) {
        case "USD":
            if (isPlural) {
                return new Array(promptBaseUrl + "miscellaneous/cents.vox");
            } else {
                return new Array(promptBaseUrl + "miscellaneous/cent.vox");
            }
        case "GBP":
            if (isPlural) {
                return new Array(promptBaseUrl + "miscellaneous/pence.vox");
            } else {
                return new Array(promptBaseUrl + "miscellaneous/penny.vox");
            }
        case "EUR":
            if (isPlural) {
                return new Array(promptBaseUrl + "miscellaneous/cents.vox");
            } else {
                return new Array(promptBaseUrl + "miscellaneous/cent.vox");
            }
        case "JPY":
            return void 0;
        case "DEM":
            if (isPlural) {
                return new Array(promptBaseUrl + "miscellaneous/pfennige.vox");
            } else {
                return new Array(promptBaseUrl + "miscellaneous/pfennig.vox");
            }
        case "ITL":
            return void 0;
        case "FRF":
            if (isPlural) {
                return new Array(propmtBaseUrl + "miscellaneous/centimes.vox");
            } else {
                return new Array(promptBaseUrl + "centime.vox");
            }
        case "ARS":
            if (isPlural) {
                return new Array(promptBaseUrl + "miscellaneous/centavos" + audioFileExtension);
            } else {
                return new Array(promptBaseUrl + "miscellaneous/centavo" + audioFileExtension);
            }
        /*
        case "CAD":
            if (isPlural) {
                return new Array("cents"); //FIXME
            } else {
                return new Array("cent"); //FIXME
            }
        case "MXN":
            if (isPlural) {
                return new Array("centavos"); //FIXME
            } else {
                return new Array("centavo"); //FIXME
            }
        case "CHF":
            if (isPlural) {
                return new Array("centimes"); //FIXME
            } else {
                return new Array("centime"); //FIXME
            }
        */
        default:
            return void 0;
    }
}

// FIXME - 800 as eight hundred
// FIXME - 408 as four oh eight

function phonePrompts(value)                                                              
{                                                                                         
  var phoneNumber;                                                                      
  var extension;                                                                        
  var xIndex;                                                                           
  xIndex = value.indexOf("x");                                                          
  if (xIndex != -1) {                                                                   
    phoneNumber = value.substring(0, xIndex);                                         
    extension = value.substring(xIndex + 1);                                          
  } else {                                                                              
      phoneNumber = value;                                                              
      extension = "";                                                                   
  }                                                                                     
                                                                                          
  var promptsArray = new Array();                                                       
  if (phoneNumber.length == 10) {     
  
    if (phoneNumber.substring(0, 2) == "11"){ // Area Metropolitana Buenos Aires (AMBA)
      promptsArray = promptsArray.concat(  
      promptBaseUrl + "miscellaneous/prefix" + audioFileExtension,
      phoneCharacteristicPrompts(phoneNumber.substring(0, 2)),                     
      promptBaseUrl + "miscellaneous/telefono" + audioFileExtension,                   
      phoneCharacteristicPrompts(phoneNumber.substring(2, 6)),                     
      promptBaseUrl + "miscellaneous/350ms" + audioFileExtension,                   
      phoneNumberPrompts(phoneNumber.substring(6)));
        
    } else if (phoneNumber.substring(0, 3) == "220") {      
  
        if (phoneNumber.charAt(3) == "2") { // Gonzalez Catan Buenos Aires   
          promptsArray = promptsArray.concat(
          promptBaseUrl + "miscellaneous/prefix" + audioFileExtension,
          phoneCharacteristicPrompts(phoneNumber.substring(0, 4)),                     
          promptBaseUrl + "miscellaneous/telefono" + audioFileExtension,                   
          phoneCharacteristicPrompts(phoneNumber.substring(4, 6)),                     
          promptBaseUrl + "miscellaneous/350ms" + audioFileExtension,                   
          phoneNumberPrompts(phoneNumber.substring(6)));          
        } else { // Merlo (Prov. Buenos Aires) Buenos Aires
            promptsArray = promptsArray.concat(
            promptBaseUrl + "miscellaneous/prefix" + audioFileExtension,
            phoneCharacteristicPrompts(phoneNumber.substring(0, 3)),                     
            promptBaseUrl + "miscellaneous/telefono" + audioFileExtension,                   
            phoneCharacteristicPrompts(phoneNumber.substring(3, 6)),                     
            promptBaseUrl + "miscellaneous/350ms" + audioFileExtension,                   
            phoneNumberPrompts(phoneNumber.substring(6)));    
        }  
  
    } else if (phoneNumber.substring(0, 3) == "221") { //La Plata Buenos Aires  
        promptsArray = promptsArray.concat(
        promptBaseUrl + "miscellaneous/prefix" + audioFileExtension,
        phoneCharacteristicPrompts(phoneNumber.substring(0, 3)),                     
        promptBaseUrl + "miscellaneous/telefono" + audioFileExtension,                   
        phoneCharacteristicPrompts(phoneNumber.substring(3, 6)),                     
        promptBaseUrl + "miscellaneous/350ms" + audioFileExtension,                   
        phoneNumberPrompts(phoneNumber.substring(6)));
        
    } else if 
        ((phoneNumber.substring(0,  4) == "2221") || //Magdalena Buenos Aires
        (phoneNumber.substring(0, 4) == "2223") || //Coronel Brandsen Buenos Aires  
        (phoneNumber.substring(0, 4) == "2224") || //Glew Buenos Aires    
        (phoneNumber.substring(0, 4) == "2225") || //Alejandro Korn Buenos Aires 
        (phoneNumber.substring(0, 4) == "2226") || //Cañuelas Buenos Aires  
        (phoneNumber.substring(0, 4) == "2227") || //Lobos Buenos Aires 
        (phoneNumber.substring(0, 4) == "2229"))  {  //Juan Maria Gutierrez Buenos Aires             
          promptsArray = promptsArray.concat(
          promptBaseUrl + "miscellaneous/prefix" + audioFileExtension,
          phoneCharacteristicPrompts(phoneNumber.substring(0, 4)),                     
          promptBaseUrl + "miscellaneous/telefono" + audioFileExtension,                   
          phoneCharacteristicPrompts(phoneNumber.substring(4, 6)),                     
          promptBaseUrl + "miscellaneous/350ms" + audioFileExtension,                   
          phoneNumberPrompts(phoneNumber.substring(6)));
        
    } else if (phoneNumber.substring(0, 3) == "223") { // Mar Del Plata Buenos  Aires      
        promptsArray = promptsArray.concat(
        promptBaseUrl + "miscellaneous/prefix" + audioFileExtension,
        phoneCharacteristicPrompts(phoneNumber.substring(0, 3)),                     
        promptBaseUrl + "miscellaneous/telefono" + audioFileExtension,                   
        phoneCharacteristicPrompts(phoneNumber.substring(3, 6)),                     
        promptBaseUrl + "miscellaneous/350ms" + audioFileExtension,                   
        phoneNumberPrompts(phoneNumber.substring(6)));
           
    } else if 
       ((phoneNumber.substring(0, 4)  == "2241") || // Chascomus Buenos Aires                               
       (phoneNumber.substring(0, 4) == "2242") || // Lezama Buenos Aires                                         
       (phoneNumber.substring(0, 4) == "2243") || // General Belgrano Buenos Aires                               
       (phoneNumber.substring(0, 4) == "2244") || // Las Flores (Prov. Buenos Aires) Buenos Aires                
       (phoneNumber.substring(0, 4) == "2245") || // Dolores (Prov. Buenos Aires) Buenos Aires                   
       (phoneNumber.substring(0, 4) == "2246") || // Santa Teresita Buenos Aires                                 
       (phoneNumber.substring(0, 4) == "2252") || // San Clemente Del Tuyu Buenos Aires                          
       (phoneNumber.substring(0, 4) == "2254") || // Pinamar Buenos Aires                                        
       (phoneNumber.substring(0, 4) == "2255") || // Villa Gesell Buenos Aires                                   
       (phoneNumber.substring(0, 4) == "2257") || // Mar De Ajo Buenos Aires                                     
       (phoneNumber.substring(0, 4) == "2261") || // Loberia Buenos Aires                                        
       (phoneNumber.substring(0, 4) == "2262") || // Necochea Buenos Aires                                       
       (phoneNumber.substring(0, 4) == "2264") || // La Dulce Buenos Aires                                       
       (phoneNumber.substring(0, 4) == "2265") || // Coronel Vidal Buenos Aires                                  
       (phoneNumber.substring(0, 4) == "2266") || // Balcarce Buenos Aires                                       
       (phoneNumber.substring(0, 4) == "2267") || // General Madariaga Buenos Aires                              
       (phoneNumber.substring(0, 4) == "2268") || // Maipu (Prov. Buenos Aires) Buenos Aires                     
       (phoneNumber.substring(0, 4) == "2271") || // Monte Buenos Aires                                          
       (phoneNumber.substring(0, 4) == "2272") || // Navarro Buenos Aires                                        
       (phoneNumber.substring(0, 4) == "2273") || // Carmen De Areco Buenos Aires                                
       (phoneNumber.substring(0, 4) == "2274") || // Carlos Spegazzini Buenos Aires                              
       (phoneNumber.substring(0, 4) == "2281") || // Azul Buenos Aires                                                      
       (phoneNumber.substring(0, 4) == "2283") || // Tapalque Buenos Aires                                       
       (phoneNumber.substring(0, 4) == "2284") || // Olavarria Buenos Aires                                      
       (phoneNumber.substring(0, 4) == "2285") || // Laprida Buenos Aires                                        
       (phoneNumber.substring(0, 4) == "2286") || // General Lamadrid Buenos Aires                               
       (phoneNumber.substring(0, 4) == "2291") || // Miramar (Prov. Buenos Aires) Buenos Aires                     
       (phoneNumber.substring(0, 4) == "2292") || // Benito Juarez Buenos Aires                                  
       (phoneNumber.substring(0, 4) == "2293") || // Tandil Buenos Aires                                         
       (phoneNumber.substring(0, 4) == "2296") || // Ayacucho Buenos Aires                                       
       (phoneNumber.substring(0, 4) == "2297") || // Rauch Buenos Aires                                          
       (phoneNumber.substring(0, 4) == "2302") || // General Pico La Pampa                                       
       (phoneNumber.substring(0, 4) == "2314") || // Bolivar Buenos Aires                                        
       (phoneNumber.substring(0, 4) == "2316") || // Daireaux Buenos Aires                                       
       (phoneNumber.substring(0, 4) == "2317") || // 9 De Julio (Prov. Buenos Aires) Buenos Aires                
       (phoneNumber.substring(0, 4) == "2320") || // Jose C. Paz Buenos Aires                                    
       (phoneNumber.substring(0, 4) == "2322") || // Pilar (Prov. Buenos Aires) Buenos Aires                     
       (phoneNumber.substring(0, 4) == "2323") || // Lujan (Prov. Buenos Aires) Buenos Aires                     
       (phoneNumber.substring(0, 4) == "2324") || // Mercedes (Prov. Buenos Aires) Buenos Aires                  
       (phoneNumber.substring(0, 4) == "2325") || // San Andres De Giles Buenos Aires                            
       (phoneNumber.substring(0, 4) == "2326") || // San Antonio De Areco Buenos Aires                           
       (phoneNumber.substring(0, 4) == "2331") || // Realico La Pampa                                            
       (phoneNumber.substring(0, 4) == "2333") || // Quemu Quemu La Pampa                                        
       (phoneNumber.substring(0, 4) == "2334") || // Eduardo Castex La Pampa                                     
       (phoneNumber.substring(0, 4) == "2335") || // Caleufu La Pampa                                            
       (phoneNumber.substring(0, 4) == "2336") || // Huinca Renanco Cordoba                                      
       (phoneNumber.substring(0, 4) == "2337") || // America Buenos Aires                                        
       (phoneNumber.substring(0, 4) == "2338") || // Victorica La Pampa                                          
       (phoneNumber.substring(0, 4) == "2342") || // Bragado Buenos Aires                                        
       (phoneNumber.substring(0, 4) == "2343") || // Norberto De La Riestra Buenos Aires                         
       (phoneNumber.substring(0, 4) == "2344") || // Saladillo (Prov. Buenos Aires) Buenos Aires                 
       (phoneNumber.substring(0, 4) == "2345") || // 25 De Mayo (Prov. Buenos Aires) Buenos Aires                
       (phoneNumber.substring(0, 4) == "2346") || // Chivilcoy Buenos Aires                                      
       (phoneNumber.substring(0, 4) == "2352") || // Chacabuco Buenos Aires                                      
       (phoneNumber.substring(0, 4) == "2353") || // General Arenales Buenos Aires                                   
       (phoneNumber.substring(0, 4) == "2354") || // Vedia Buenos Aires                                          
       (phoneNumber.substring(0, 4) == "2355") || // Lincoln Buenos Aires                                        
       (phoneNumber.substring(0, 4) == "2356") || // General Pinto Buenos Aires                                  
       (phoneNumber.substring(0, 4) == "2357") || // Carlos Tejedor Buenos Aires                                 
       (phoneNumber.substring(0, 4) == "2358") || // Los Toldos (Prov. Buenos Aires) Buenos Aires                
       (phoneNumber.substring(0, 4) == "2362")) { // Junin (Prov. Buenos Aires) Buenos Aires              
         promptsArray = promptsArray.concat(
         promptBaseUrl + "miscellaneous/prefix" + audioFileExtension,
         phoneCharacteristicPrompts(phoneNumber.substring(0, 4)),                     
         promptBaseUrl + "miscellaneous/telefono" + audioFileExtension,                   
         phoneCharacteristicPrompts(phoneNumber.substring(4, 6)),                     
         promptBaseUrl + "miscellaneous/350ms" + audioFileExtension,                   
         phoneNumberPrompts(phoneNumber.substring(6)));
        
    } else if (phoneNumber.substring(0, 3) == "237") { // Moreno Buenos Aires        
        promptsArray = promptsArray.concat(
        promptBaseUrl + "miscellaneous/prefix" + audioFileExtension,
        phoneCharacteristicPrompts(phoneNumber.substring(0, 3)),                     
        promptBaseUrl + "miscellaneous/telefono" + audioFileExtension,                   
        phoneCharacteristicPrompts(phoneNumber.substring(3, 6)),                     
        promptBaseUrl + "miscellaneous/350ms" + audioFileExtension,                   
        phoneNumberPrompts(phoneNumber.substring(6)));  
          
    } else if 
       ((phoneNumber.substring(0, 4)  == "2392") || // Trenque Lauquen Buenos Aires                                 
       (phoneNumber.substring(0, 4) == "2393") || // Salazar Buenos Aires                                         
       (phoneNumber.substring(0, 4) == "2394") || // Tres Lomas Buenos Aires                                      
       (phoneNumber.substring(0, 4) == "2395") || // Carlos Casares Buenos Aires                                  
       (phoneNumber.substring(0, 4) == "2396") || // Pehuajo Buenos Aires                                         
       (phoneNumber.substring(0, 4) == "2473") || // Colon (Prov. Buenos Aires) Buenos Aires                      
       (phoneNumber.substring(0, 4) == "2474") || // Salto Buenos Aires                                           
       (phoneNumber.substring(0, 4) == "2475") || // Rojas Buenos Aires                                           
       (phoneNumber.substring(0, 4) == "2477") || // Pergamino Buenos Aires                                       
       (phoneNumber.substring(0, 4) == "2478")) { // Arrecifes Buenos Aires  
         promptsArray = promptsArray.concat(
         promptBaseUrl + "miscellaneous/prefix" + audioFileExtension,
         phoneCharacteristicPrompts(phoneNumber.substring(0, 4)),                     
         promptBaseUrl + "miscellaneous/telefono" + audioFileExtension,                   
         phoneCharacteristicPrompts(phoneNumber.substring(4, 6)),                     
         promptBaseUrl + "miscellaneous/350ms" + audioFileExtension,                   
         phoneNumberPrompts(phoneNumber.substring(6)));   
       
    } else if (phoneNumber.substring(0, 3) == "261") { // Mendoza Mendoza              
        promptsArray = promptsArray.concat(
        promptBaseUrl + "miscellaneous/prefix" + audioFileExtension,
        phoneCharacteristicPrompts(phoneNumber.substring(0, 3)),                     
        promptBaseUrl + "miscellaneous/telefono" + audioFileExtension,                   
        phoneCharacteristicPrompts(phoneNumber.substring(3, 6)),                     
        promptBaseUrl + "miscellaneous/350ms" + audioFileExtension,                   
        phoneNumberPrompts(phoneNumber.substring(6)));  
        
    } else if 
       ((phoneNumber.substring(0, 4)  == "2622") || // Tunuyan Mendoza                                                           
       (phoneNumber.substring(0, 4) == "2623") || // San Martin (Prov. Mendoza) Mendoza                                        
       (phoneNumber.substring(0, 4) == "2624") || // Uspallata Mendoza                                                         
       (phoneNumber.substring(0, 4) == "2625") || // General Alvear (Prov. Mendoza) Mendoza                                    
       (phoneNumber.substring(0, 4) == "2626") || // La Paz (Prov. Mendoza) Mendoza                                            
       (phoneNumber.substring(0, 4) == "2627")) { // San Rafael (Prov. Mendoza) Mendoza                                        
         promptsArray = promptsArray.concat(
         promptBaseUrl + "miscellaneous/prefix" + audioFileExtension,
         phoneCharacteristicPrompts(phoneNumber.substring(0, 4)),                     
         promptBaseUrl + "miscellaneous/telefono" + audioFileExtension,                   
         phoneCharacteristicPrompts(phoneNumber.substring(4, 6)),                     
         promptBaseUrl + "miscellaneous/350ms" + audioFileExtension,                   
         phoneNumberPrompts(phoneNumber.substring(6)));                                            

    } else if (phoneNumber.substring(0, 3) == "264") {        
        if 
          ((phoneNumber.charAt(3) == "6") || // San Agustin Del Valle Fertil San Juan                
          (phoneNumber.charAt(3)  == "7") || // Jachal San Juan                                      
          (phoneNumber.charAt(3)  == "8"))  {  // Calingasta San Juan                                   
            promptsArray = promptsArray.concat(
            promptBaseUrl + "miscellaneous/prefix" + audioFileExtension,
            phoneCharacteristicPrompts(phoneNumber.substring(0, 4)),                     
            promptBaseUrl + "miscellaneous/telefono" + audioFileExtension,                   
            phoneCharacteristicPrompts(phoneNumber.substring(4, 6)),                     
            promptBaseUrl + "miscellaneous/350ms" + audioFileExtension,                   
            phoneNumberPrompts(phoneNumber.substring(6)));          
        } else { // San Juan San Juan
            promptsArray = promptsArray.concat(
            promptBaseUrl + "miscellaneous/prefix" + audioFileExtension,
            phoneCharacteristicPrompts(phoneNumber.substring(0, 3)),                     
            promptBaseUrl + "miscellaneous/telefono" + audioFileExtension,                   
            phoneCharacteristicPrompts(phoneNumber.substring(3, 6)),                     
            promptBaseUrl + "miscellaneous/350ms" + audioFileExtension,                   
            phoneNumberPrompts(phoneNumber.substring(6)));    
        } 
         
    } else if 
       ((phoneNumber.substring(0, 4)  == "2651") || // San Francisco Del Monte De Oro San Luis                                   
       (phoneNumber.substring(0, 4) == "2652") || // San Luis (Prov. San Luis) San Luis                                        
       (phoneNumber.substring(0, 4) == "2655") || // La Toma San Luis                                                          
       (phoneNumber.substring(0, 4) == "2656") || // Tilisarao San Luis                                                        
       (phoneNumber.substring(0, 4) == "2657") || // Mercedes (Prov. San Luis) San Luis                                        
       (phoneNumber.substring(0, 4) == "2658") || // Buena Esperanza San Luis                                                  
       (phoneNumber.substring(0, 4) == "2901") || // Ushuaia Tierra Del Fuego                                                  
       (phoneNumber.substring(0, 4) == "2902") || // Rio Turbio Santa Cruz                                                     
       (phoneNumber.substring(0, 4) == "2903")) { // Rio Mayo Chubut                                      
         promptsArray = promptsArray.concat(
         promptBaseUrl + "miscellaneous/prefix" + audioFileExtension,
         phoneCharacteristicPrompts(phoneNumber.substring(0, 4)),                     
         promptBaseUrl + "miscellaneous/telefono" + audioFileExtension,                   
         phoneCharacteristicPrompts(phoneNumber.substring(4, 6)),                     
         promptBaseUrl + "miscellaneous/350ms" + audioFileExtension,                   
         phoneNumberPrompts(phoneNumber.substring(6)));                                     

    } else if (phoneNumber.substring(0, 3) == "291") { // Bahia Blanca Buenos Aires                  
        promptsArray = promptsArray.concat(
        promptBaseUrl + "miscellaneous/prefix" + audioFileExtension,
        phoneCharacteristicPrompts(phoneNumber.substring(0, 3)),                     
        promptBaseUrl + "miscellaneous/telefono" + audioFileExtension,                   
        phoneCharacteristicPrompts(phoneNumber.substring(3, 6)),                     
        promptBaseUrl + "miscellaneous/350ms" + audioFileExtension,                   
        phoneNumberPrompts(phoneNumber.substring(6)));
        
    } else if 
       ((phoneNumber.substring(0, 4)  == "2920") || // Viedma Rio Negro                                            
       (phoneNumber.substring(0, 4) == "2921") || // Coronel Dorrego Buenos Aires                                
       (phoneNumber.substring(0, 4) == "2922") || // Coronel Pringles Buenos Aires                               
       (phoneNumber.substring(0, 4) == "2923") || // Pigüe Buenos Aires                                          
       (phoneNumber.substring(0, 4) == "2924") || // Darregueira Buenos Aires                                    
       (phoneNumber.substring(0, 4) == "2925") || // Villa Iris Buenos Aires                                     
       (phoneNumber.substring(0, 4) == "2926") || // Coronel Suarez Buenos Aires                                 
       (phoneNumber.substring(0, 4) == "2927") || // Medanos (Prov. Buenos Aires) Buenos Aires                   
       (phoneNumber.substring(0, 4) == "2928") || // Pedro Luro Buenos Aires                                     
       (phoneNumber.substring(0, 4) == "2929") || // Guamini Buenos Aires                                        
       (phoneNumber.substring(0, 4) == "2931") || // Rio Colorado (Prov. Rio Negro) Rio Negro                    
       (phoneNumber.substring(0, 4) == "2932") || // Punta Alta Buenos Aires                                     
       (phoneNumber.substring(0, 4) == "2933") || // Huanguelen Sur Buenos Aires                                 
       (phoneNumber.substring(0, 4) == "2934") || // San Antonio Oeste Rio Negro                                 
       (phoneNumber.substring(0, 4) == "2935") || // Rivera Buenos Aires                                         
       (phoneNumber.substring(0, 4) == "2936") || // Carhue Buenos Aires                                         
       (phoneNumber.substring(0, 4) == "2940") || // Ingeniero Jacobacci Rio Negro                               
       (phoneNumber.substring(0, 4) == "2941") || // General Roca (Prov. Rio Negro) Rio Negro                    
       (phoneNumber.substring(0, 4) == "2942") || // Zapala Neuquen                                              
       (phoneNumber.substring(0, 4) == "2944") || // San Carlos De Bariloche Rio Negro                           
       (phoneNumber.substring(0, 4) == "2945") || // Esquel Chubut                                               
       (phoneNumber.substring(0, 4) == "2946") || // Choele Choel Rio Negro                                      
       (phoneNumber.substring(0, 4) == "2948") || // Chos Malal Neuquen                                          
       (phoneNumber.substring(0, 4) == "2952") || // General Acha La Pampa                                       
       (phoneNumber.substring(0, 4) == "2953") || // Macachin La Pampa                                           
       (phoneNumber.substring(0, 4) == "2954") || // Santa Rosa (Prov. La Pampa) La Pampa                        
       (phoneNumber.substring(0, 4) == "2962") || // San Julian Santa Cruz                                       
       (phoneNumber.substring(0, 4) == "2963") || // Perito Moreno Santa Cruz                                      
       (phoneNumber.substring(0, 4) == "2964") || // Rio Grande (Prov. Tierra Del Fuego) Tierra Del Fuego        
       (phoneNumber.substring(0, 4) == "2965") || // Trelew Chubut                                        
       (phoneNumber.substring(0, 4) == "2966")) { //Rio Gallegos Santa Cruz    
         promptsArray = promptsArray.concat(
         promptBaseUrl + "miscellaneous/prefix" + audioFileExtension,
         phoneCharacteristicPrompts(phoneNumber.substring(0, 4)),                     
         promptBaseUrl + "miscellaneous/telefono" + audioFileExtension,                   
         phoneCharacteristicPrompts(phoneNumber.substring(4, 6)),                     
         promptBaseUrl + "miscellaneous/350ms" + audioFileExtension,                   
         phoneNumberPrompts(phoneNumber.substring(6)));                                           
        
    } else if (phoneNumber.substring(0, 3) == "297") {        
        if (phoneNumber.charAt(3) == "2") { // San Martin De Los Andes Neuquen         
          promptsArray = promptsArray.concat(
          promptBaseUrl + "miscellaneous/prefix" + audioFileExtension,
          phoneCharacteristicPrompts(phoneNumber.substring(0, 4)),                     
          promptBaseUrl + "miscellaneous/telefono" + audioFileExtension,                   
          phoneCharacteristicPrompts(phoneNumber.substring(4, 6)),                     
          promptBaseUrl + "miscellaneous/350ms" + audioFileExtension,                   
          phoneNumberPrompts(phoneNumber.substring(6)));          
        } else { // Comodoro Rivadavia Chubut     
            promptsArray = promptsArray.concat(
            promptBaseUrl + "miscellaneous/prefix" + audioFileExtension,
            phoneCharacteristicPrompts(phoneNumber.substring(0, 3)),                     
            promptBaseUrl + "miscellaneous/telefono" + audioFileExtension,                   
            phoneCharacteristicPrompts(phoneNumber.substring(3, 6)),                     
            promptBaseUrl + "miscellaneous/350ms" + audioFileExtension,                   
            phoneNumberPrompts(phoneNumber.substring(6)));    
        } 

    } else if 
       ((phoneNumber.substring(0, 4)  == "2982") || // Viedma Rio Negro                                            
       (phoneNumber.substring(0, 4) == "2983")) { // Coronel Dorrego Buenos Aires 
       	 promptsArray = promptsArray.concat(
         promptBaseUrl + "miscellaneous/prefix" + audioFileExtension,
         phoneCharacteristicPrompts(phoneNumber.substring(0, 4)),                     
         promptBaseUrl + "miscellaneous/telefono" + audioFileExtension,                   
         phoneCharacteristicPrompts(phoneNumber.substring(4, 6)),                     
         promptBaseUrl + "miscellaneous/350ms" + audioFileExtension,                   
         phoneNumberPrompts(phoneNumber.substring(6)));

    } else if (phoneNumber.substring(0, 3) == "299") { // Neuquen Neuquen                   
        promptsArray = promptsArray.concat(
        promptBaseUrl + "miscellaneous/prefix" + audioFileExtension,
        phoneCharacteristicPrompts(phoneNumber.substring(0, 3)),                     
        promptBaseUrl + "miscellaneous/telefono" + audioFileExtension,                   
        phoneCharacteristicPrompts(phoneNumber.substring(3, 6)),                     
        promptBaseUrl + "miscellaneous/350ms" + audioFileExtension,                   
        phoneNumberPrompts(phoneNumber.substring(6)));  

    } else if 
       ((phoneNumber.substring(0, 4)  == "3327") || // Lopez Camelo Buenos Aires                                              
       (phoneNumber.substring(0, 4) == "3329") || // San Pedro (Prov. Buenos Aires) Buenos Aires                            
       (phoneNumber.substring(0, 4) == "3382") || // Rufino Santa Fe                                                        
       (phoneNumber.substring(0, 4) == "3385") || // Laboulaye Cordoba                                                      
       (phoneNumber.substring(0, 4) == "3387") || // Bouchard Cordoba                                                       
       (phoneNumber.substring(0, 4) == "3388") || // General Villegas Buenos Aires                                          
       (phoneNumber.substring(0, 4) == "3400") || // Villa Constitucion Santa Fe                                            
       (phoneNumber.substring(0, 4) == "3401") || // El Trebol Santa Fe                                                     
       (phoneNumber.substring(0, 4) == "3402") || // Arroyo Seco Santa Fe                                                   
       (phoneNumber.substring(0, 4) == "3404") || // San Carlos Centro Santa Fe                                             
       (phoneNumber.substring(0, 4) == "3405") || // San Javier (Prov. Santa Fe) Santa Fe                                   
       (phoneNumber.substring(0, 4) == "3406") || // San Jorge (Prov. Santa Fe) Santa Fe                                    
       (phoneNumber.substring(0, 4) == "3407") || // Ramallo Buenos Aires                                                   
       (phoneNumber.substring(0, 4) == "3408") || // San Cristobal Santa Fe                                                 
       (phoneNumber.substring(0, 4) == "3409")) { // Moises Ville Santa Fe                                                  
         promptsArray = promptsArray.concat(
         promptBaseUrl + "miscellaneous/prefix" + audioFileExtension,
         phoneCharacteristicPrompts(phoneNumber.substring(0, 4)),                     
         promptBaseUrl + "miscellaneous/telefono" + audioFileExtension,                   
         phoneCharacteristicPrompts(phoneNumber.substring(4, 6)),                     
         promptBaseUrl + "miscellaneous/350ms" + audioFileExtension,                   
         phoneNumberPrompts(phoneNumber.substring(6)));

    } else if 
    	  ((phoneNumber.substring(0, 3) == "341") || // Rosario Santa Fe    
    	  (phoneNumber.substring(0, 3) == "342")) { // Santa Fe Santa Fe      
    	    promptsArray = promptsArray.concat(
          promptBaseUrl + "miscellaneous/prefix" + audioFileExtension,
          phoneCharacteristicPrompts(phoneNumber.substring(0, 3)),                     
          promptBaseUrl + "miscellaneous/telefono" + audioFileExtension,                   
          phoneCharacteristicPrompts(phoneNumber.substring(3, 6)),                     
          promptBaseUrl + "miscellaneous/350ms" + audioFileExtension,                   
          phoneNumberPrompts(phoneNumber.substring(6)));
    	  
    } else if (phoneNumber.substring(0, 3) == "343") {        
        if 
          ((phoneNumber.charAt(3) == "5") || // Nogoya Entre Rios                   
          (phoneNumber.charAt(3)  == "6") || // Victoria Entre Rios     
          (phoneNumber.charAt(3)  == "7") || // La Paz (Prov. Entre Rios) Entre Rios                                    
          (phoneNumber.charAt(3)  == "8")) {  // Bovril Entre Rios                                      
            promptsArray = promptsArray.concat(
            promptBaseUrl + "miscellaneous/prefix" + audioFileExtension,
            phoneCharacteristicPrompts(phoneNumber.substring(0, 4)),                     
            promptBaseUrl + "miscellaneous/telefono" + audioFileExtension,                   
            phoneCharacteristicPrompts(phoneNumber.substring(4, 6)),                     
            promptBaseUrl + "miscellaneous/350ms" + audioFileExtension,                   
            phoneNumberPrompts(phoneNumber.substring(6)));          
        } else { // Parana Entre Rios   
            promptsArray = promptsArray.concat(
            promptBaseUrl + "miscellaneous/prefix" + audioFileExtension,
            phoneCharacteristicPrompts(phoneNumber.substring(0, 3)),                     
            promptBaseUrl + "miscellaneous/telefono" + audioFileExtension,                   
            phoneCharacteristicPrompts(phoneNumber.substring(3, 6)),                     
            promptBaseUrl + "miscellaneous/350ms" + audioFileExtension,                   
            phoneNumberPrompts(phoneNumber.substring(6)));    
        } 
    	                         
    } else if 
       ((phoneNumber.substring(0, 4)  == "3442") || // Concepcion Del Uruguay Entre Rios                                                
       (phoneNumber.substring(0, 4) == "3444") || // Gualeguay Entre Rios                             
       (phoneNumber.substring(0, 4) == "3445") || // Rosario Del Tala Entre Rios                                                         
       (phoneNumber.substring(0, 4) == "3446") || // Gualeguaychu Entre Rios                                                         
       (phoneNumber.substring(0, 4) == "3447")) { // Colon (Prov. Entre Rios) Entre Rios                                                         
         promptsArray = promptsArray.concat(
         promptBaseUrl + "miscellaneous/prefix" + audioFileExtension,
         phoneCharacteristicPrompts(phoneNumber.substring(0, 4)),                     
         promptBaseUrl + "miscellaneous/telefono" + audioFileExtension,                   
         phoneCharacteristicPrompts(phoneNumber.substring(4, 6)),                     
         promptBaseUrl + "miscellaneous/350ms" + audioFileExtension,                   
         phoneNumberPrompts(phoneNumber.substring(6)));  

    } else if (phoneNumber.substring(0, 3) == "345") {      
        if 
          ((phoneNumber.charAt(3) == "4") || // Federal Entre Rios                      
          (phoneNumber.charAt(3)  == "5") || // Villaguay Entre Rios      
          (phoneNumber.charAt(3)  == "6") || // Chajari Entre Rios                                      
          (phoneNumber.charAt(3)  == "8")) {  // San Jose De Feliciano Entre Rios                                         
            promptsArray = promptsArray.concat(
            promptBaseUrl + "miscellaneous/prefix" + audioFileExtension,
            phoneCharacteristicPrompts(phoneNumber.substring(0, 4)),                     
            promptBaseUrl + "miscellaneous/telefono" + audioFileExtension,                   
            phoneCharacteristicPrompts(phoneNumber.substring(4, 6)),                     
            promptBaseUrl + "miscellaneous/350ms" + audioFileExtension,                   
            phoneNumberPrompts(phoneNumber.substring(6)));          
        } else { // Concordia Entre Rios                                    
            promptsArray = promptsArray.concat(
            promptBaseUrl + "miscellaneous/prefix" + audioFileExtension,
            phoneCharacteristicPrompts(phoneNumber.substring(0, 3)),                     
            promptBaseUrl + "miscellaneous/telefono" + audioFileExtension,                   
            phoneCharacteristicPrompts(phoneNumber.substring(3, 6)),                     
            promptBaseUrl + "miscellaneous/350ms" + audioFileExtension,                   
            phoneNumberPrompts(phoneNumber.substring(6)));
        }        

    } else if 
       ((phoneNumber.substring(0, 4)  == "3460") || // Santa Teresa (Prov. Santa Fe) Santa Fe                                        
       (phoneNumber.substring(0, 4) == "3461") || // San Nicolas Buenos Aires                                    
       (phoneNumber.substring(0, 4) == "3462") || // Venado Tuerto Santa Fe                                      
       (phoneNumber.substring(0, 4) == "3463") || // Canals Cordoba                                              
       (phoneNumber.substring(0, 4) == "3464") || // Casilda Santa Fe                                            
       (phoneNumber.substring(0, 4) == "3465") || // Firmat Santa Fe                                             
       (phoneNumber.substring(0, 4) == "3466") || // Barrancas (Prov. Santa Fe) Santa Fe                         
       (phoneNumber.substring(0, 4) == "3467") || // Cruz Alta Cordoba                                           
       (phoneNumber.substring(0, 4) == "3468") || // Corral De Bustos Cordoba                                    
       (phoneNumber.substring(0, 4) == "3469") || // Acebal Santa Fe                                             
       (phoneNumber.substring(0, 4) == "3471") || // Cañada De Gomez Santa Fe                                    
       (phoneNumber.substring(0, 4) == "3472") || // Marcos Juarez Cordoba                                       
       (phoneNumber.substring(0, 4) == "3476") || // San Lorenzo (Prov. Santa Fe) Santa Fe                       
       (phoneNumber.substring(0, 4) == "3482") || // Reconquista Santa Fe                                        
       (phoneNumber.substring(0, 4) == "3483") || // Vera Santa Fe                                               
       (phoneNumber.substring(0, 4) == "3487") || // Zarate Buenos Aires                                         
       (phoneNumber.substring(0, 4) == "3488") || // Escobar Buenos Aires                                        
       (phoneNumber.substring(0, 4) == "3489") || // Campana Buenos Aires                                        
       (phoneNumber.substring(0, 4) == "3491") || // Ceres Santa Fe                                              
       (phoneNumber.substring(0, 4) == "3492") || // Rafaela Santa Fe                                            
       (phoneNumber.substring(0, 4) == "3493") || // Sunchales Santa Fe                                          
       (phoneNumber.substring(0, 4) == "3496") || // Esperanza (Prov. Santa Fe) Santa Fe                         
       (phoneNumber.substring(0, 4) == "3497") || // Llambi Campbell Santa Fe                                    
       (phoneNumber.substring(0, 4) == "3498")) { // San Justo (Prov. Santa Fe) Santa Fe  
       	  promptsArray = promptsArray.concat(
          promptBaseUrl + "miscellaneous/prefix" + audioFileExtension,
          phoneCharacteristicPrompts(phoneNumber.substring(0, 4)),                     
          promptBaseUrl + "miscellaneous/telefono" + audioFileExtension,                   
          phoneCharacteristicPrompts(phoneNumber.substring(4, 6)),                     
          promptBaseUrl + "miscellaneous/350ms" + audioFileExtension,                   
          phoneNumberPrompts(phoneNumber.substring(6)));                       

    } else if (phoneNumber.substring(0, 3) == "351") { // Cordoba Cordoba                        
        promptsArray = promptsArray.concat(
        promptBaseUrl + "miscellaneous/prefix" + audioFileExtension,
        phoneCharacteristicPrompts(phoneNumber.substring(0, 3)),                     
        promptBaseUrl + "miscellaneous/telefono" + audioFileExtension,                   
        phoneCharacteristicPrompts(phoneNumber.substring(3, 6)),                     
        promptBaseUrl + "miscellaneous/350ms" + audioFileExtension,                   
        phoneNumberPrompts(phoneNumber.substring(6)));  

    } else if 
       ((phoneNumber.substring(0, 4)  == "3521") || // Dean Funes Cordoba                                          
       (phoneNumber.substring(0, 4) == "3522") || // Villa De Maria De Rio Seco Cordoba                                                 
       (phoneNumber.substring(0, 4) == "3524") || // Villa Del Totoral Cordoba                                                          
       (phoneNumber.substring(0, 4) == "3525")) { // Jesus Maria Cordoba
       	   promptsArray = promptsArray.concat(
           promptBaseUrl + "miscellaneous/prefix" + audioFileExtension,
           phoneCharacteristicPrompts(phoneNumber.substring(0, 4)),                     
           promptBaseUrl + "miscellaneous/telefono" + audioFileExtension,                   
           phoneCharacteristicPrompts(phoneNumber.substring(4, 6)),                     
           promptBaseUrl + "miscellaneous/350ms" + audioFileExtension,                   
           phoneNumberPrompts(phoneNumber.substring(6)));                                                                

    } else if (phoneNumber.substring(0, 3) == "353") {        
        if 
          ((phoneNumber.charAt(3) == "2") || // Oliva Cordoba                 
          (phoneNumber.charAt(3)  == "3") || // Las Varillas Cordoba                                           
          (phoneNumber.charAt(3)  == "4"))  {  // Bell Ville Cordoba                                       
            promptsArray = promptsArray.concat(
            promptBaseUrl + "miscellaneous/prefix" + audioFileExtension,
            phoneCharacteristicPrompts(phoneNumber.substring(0, 4)),                     
            promptBaseUrl + "miscellaneous/telefono" + audioFileExtension,                   
            phoneCharacteristicPrompts(phoneNumber.substring(4, 6)),                     
            promptBaseUrl + "miscellaneous/350ms" + audioFileExtension,                   
            phoneNumberPrompts(phoneNumber.substring(6)));          
        } else { // Villa Maria Cordoba  
            promptsArray = promptsArray.concat(
            promptBaseUrl + "miscellaneous/prefix" + audioFileExtension,
            phoneCharacteristicPrompts(phoneNumber.substring(0, 3)),                     
            promptBaseUrl + "miscellaneous/telefono" + audioFileExtension,                   
            phoneCharacteristicPrompts(phoneNumber.substring(3, 6)),                     
            promptBaseUrl + "miscellaneous/350ms" + audioFileExtension,                   
            phoneNumberPrompts(phoneNumber.substring(6)));    
        } 

    } else if 
       ((phoneNumber.substring(0, 4)  == "3541") || // Villa Carlos Paz Cordoba                                            
       (phoneNumber.substring(0, 4) == "3542") || // Salsacate Cordoba                                                         
       (phoneNumber.substring(0, 4) == "3543") || // Argüello Cordoba                                                          
       (phoneNumber.substring(0, 4) == "3544") || // Villa Dolores Cordoba                                                     
       (phoneNumber.substring(0, 4) == "3546") || // Santa Rosa De Calamuchita Cordoba                                         
       (phoneNumber.substring(0, 4) == "3547") || // Alta Gracia Cordoba                                                       
       (phoneNumber.substring(0, 4) == "3548") || // La Falda Cordoba                                                          
       (phoneNumber.substring(0, 4) == "3549") || // Cruz Del Eje Cordoba                                                      
       (phoneNumber.substring(0, 4) == "3562") || // Morteros Cordoba                                                          
       (phoneNumber.substring(0, 4) == "3563") || // Balnearia Cordoba                                                         
       (phoneNumber.substring(0, 4) == "3564") || // San Francisco (Prov. Cordoba) Cordoba                                     
       (phoneNumber.substring(0, 4) == "3571") || // Rio Tercero Cordoba                                                       
       (phoneNumber.substring(0, 4) == "3572") || // Rio Segundo Cordoba                                                       
       (phoneNumber.substring(0, 4) == "3573") || // Villa Del Rosario (Prov. Cordoba) Cordoba                                 
       (phoneNumber.substring(0, 4) == "3574") || // Rio Primero Cordoba                                                       
       (phoneNumber.substring(0, 4) == "3575") || // La Puerta (Prov. Cordoba) Cordoba                                         
       (phoneNumber.substring(0, 4) == "3576")) { // Arroyito (Prov. Cordoba) Cordoba                                              
          promptsArray = promptsArray.concat(
          promptBaseUrl + "miscellaneous/prefix" + audioFileExtension,
          phoneCharacteristicPrompts(phoneNumber.substring(0, 4)),                     
          promptBaseUrl + "miscellaneous/telefono" + audioFileExtension,                   
          phoneCharacteristicPrompts(phoneNumber.substring(4, 6)),                     
          promptBaseUrl + "miscellaneous/350ms" + audioFileExtension,                   
          phoneNumberPrompts(phoneNumber.substring(6)));

    } else if (phoneNumber.substring(0, 3) == "358") {      
        if 
          ((phoneNumber.charAt(3) == "2") || // Sampacho Cordoba                       
          (phoneNumber.charAt(3)  == "3") || // Vicuña Mackenna Cordoba       
          (phoneNumber.charAt(3)  == "4") || // La Carlota Cordoba                                         
          (phoneNumber.charAt(3)  == "5")) {  // Adelia Maria Cordoba                                             
            promptsArray = promptsArray.concat(
            promptBaseUrl + "miscellaneous/prefix" + audioFileExtension,
            phoneCharacteristicPrompts(phoneNumber.substring(0, 4)),                     
            promptBaseUrl + "miscellaneous/telefono" + audioFileExtension,                   
            phoneCharacteristicPrompts(phoneNumber.substring(4, 6)),                     
            promptBaseUrl + "miscellaneous/350ms" + audioFileExtension,                   
            phoneNumberPrompts(phoneNumber.substring(6)));          
        } else { // Rio Cuarto Cordoba                                        
            promptsArray = promptsArray.concat(
            promptBaseUrl + "miscellaneous/prefix" + audioFileExtension,
            phoneCharacteristicPrompts(phoneNumber.substring(0, 3)),                     
            promptBaseUrl + "miscellaneous/telefono" + audioFileExtension,                   
            phoneCharacteristicPrompts(phoneNumber.substring(3, 6)),                     
            promptBaseUrl + "miscellaneous/350ms" + audioFileExtension,                   
            phoneNumberPrompts(phoneNumber.substring(6)));
        }     

    } else if 
       ((phoneNumber.substring(0, 4)  == "3711") || // Ingeniero Guillermo N. Juarez Formosa                                   
       (phoneNumber.substring(0, 4) == "3715") || // Las Lomitas Formosa                                         
       (phoneNumber.substring(0, 4) == "3716") || // Ibarreta Formosa                                            
       (phoneNumber.substring(0, 4) == "3717") || // Formosa Formosa                                             
       (phoneNumber.substring(0, 4) == "3718") || // Clorinda Formosa                                            
       (phoneNumber.substring(0, 4) == "3721") || // Charadai Chaco                                              
       (phoneNumber.substring(0, 4) == "3722") || // Resistencia Chaco                                           
       (phoneNumber.substring(0, 4) == "3725") || // General Jose De San Martin Chaco                            
       (phoneNumber.substring(0, 4) == "3731") || // Charata Chaco                                               
       (phoneNumber.substring(0, 4) == "3732") || // Presidencia Roque Saenz Peña Chaco                          
       (phoneNumber.substring(0, 4) == "3734") || // Presidencia De La Plaza Chaco                               
       (phoneNumber.substring(0, 4) == "3735") || // Villa Angela Chaco                                          
       (phoneNumber.substring(0, 4) == "3741") || // Bernardo De Irigoyen Misiones                               
       (phoneNumber.substring(0, 4) == "3743") || // Puerto Rico Misiones                                        
       (phoneNumber.substring(0, 4) == "3751") || // Eldorado Misiones                                           
       (phoneNumber.substring(0, 4) == "3752") || // Posadas Misiones                                            
       (phoneNumber.substring(0, 4) == "3754") || // Leandro N. Alem (Prov. Misiones) Misiones                   
       (phoneNumber.substring(0, 4) == "3755") || // Obera Misiones                                              
       (phoneNumber.substring(0, 4) == "3756") || // Santo Tome Corrientes                                       
       (phoneNumber.substring(0, 4) == "3757") || // Puerto Iguazu Misiones                                      
       (phoneNumber.substring(0, 4) == "3758") || // Apostoles Misiones                                          
       (phoneNumber.substring(0, 4) == "3772") || // Paso De Los Libres Corrientes                               
       (phoneNumber.substring(0, 4) == "3773") || // Mercedes (Prov. Corrientes) Corrientes                      
       (phoneNumber.substring(0, 4) == "3774") || // Curuzu Cuatia Corrientes                                    
       (phoneNumber.substring(0, 4) == "3775") || // Monte Caseros Corrientes                                    
       (phoneNumber.substring(0, 4) == "3777") || // Goya Corrientes                                             
       (phoneNumber.substring(0, 4) == "3781") || // Caa Cati Corrientes                                         
       (phoneNumber.substring(0, 4) == "3782") || // Saladas Corrientes                                            
       (phoneNumber.substring(0, 4) == "3783") || // Corrientes Corrientes                                       
       (phoneNumber.substring(0, 4) == "3786")) { // Ituzaingo Corrientes   
       	  promptsArray = promptsArray.concat(
          promptBaseUrl + "miscellaneous/prefix" + audioFileExtension,
          phoneCharacteristicPrompts(phoneNumber.substring(0, 4)),                     
          promptBaseUrl + "miscellaneous/telefono" + audioFileExtension,                   
          phoneCharacteristicPrompts(phoneNumber.substring(4, 6)),                     
          promptBaseUrl + "miscellaneous/350ms" + audioFileExtension,                   
          phoneNumberPrompts(phoneNumber.substring(6)));                                     

    } else if (phoneNumber.substring(0, 3) == "381") { // San Miguel De Tucuman Tucuman                  
        promptsArray = promptsArray.concat(
        promptBaseUrl + "miscellaneous/prefix" + audioFileExtension,
        phoneCharacteristicPrompts(phoneNumber.substring(0, 3)),                     
        promptBaseUrl + "miscellaneous/telefono" + audioFileExtension,                   
        phoneCharacteristicPrompts(phoneNumber.substring(3, 6)),                     
        promptBaseUrl + "miscellaneous/350ms" + audioFileExtension,                   
        phoneNumberPrompts(phoneNumber.substring(6)));

    } else if 
       ((phoneNumber.substring(0, 4)  == "3821") || // Chepes La Rioja                                     
       (phoneNumber.substring(0, 4) == "3822") || // La Rioja La Rioja                                                            
       (phoneNumber.substring(0, 4) == "3825") || // Chilecito La Rioja                                                           
       (phoneNumber.substring(0, 4) == "3826") || // Chamical La Rioja                                                            
       (phoneNumber.substring(0, 4) == "3827") || // Aimogasta La Rioja                                                           
       (phoneNumber.substring(0, 4) == "3832") || // Recreo (Prov. Catamarca) Catamarca                                           
       (phoneNumber.substring(0, 4) == "3833") || // Catamarca Catamarca                                                          
       (phoneNumber.substring(0, 4) == "3835") || // Andalgala Catamarca                                                          
       (phoneNumber.substring(0, 4) == "3837") || // Tinogasta Catamarca                                                          
       (phoneNumber.substring(0, 4) == "3838") || // Santa Maria (Prov. Catamarca) Catamarca                                      
       (phoneNumber.substring(0, 4) == "3841") || // Monte Quemado Santiago Del Estero                                            
       (phoneNumber.substring(0, 4) == "3843") || // Quimili Santiago Del Estero                                                  
       (phoneNumber.substring(0, 4) == "3844") || // A#atuya Santiago Del Estero                                                  
       (phoneNumber.substring(0, 4) == "3845") || // Loreto Santiago Del Estero                                                   
       (phoneNumber.substring(0, 4) == "3846")) { // Tintina Santiago Del Estero     
       	  promptsArray = promptsArray.concat(
          promptBaseUrl + "miscellaneous/prefix" + audioFileExtension,
          phoneCharacteristicPrompts(phoneNumber.substring(0, 4)),                     
          promptBaseUrl + "miscellaneous/telefono" + audioFileExtension,                   
          phoneCharacteristicPrompts(phoneNumber.substring(4, 6)),                     
          promptBaseUrl + "miscellaneous/350ms" + audioFileExtension,                   
          phoneNumberPrompts(phoneNumber.substring(6)));                                             

    } else if (phoneNumber.substring(0, 3) == "385") {      
        if 
          ((phoneNumber.charAt(3) == "4") || // Frias Santiago Del Estero                                            
          (phoneNumber.charAt(3)  == "5") || // Suncho Corral Santiago Del Estero                      
          (phoneNumber.charAt(3)  == "6") || // Ojo De Agua (Santiago Del Estero) Santiago Del Estero
          (phoneNumber.charAt(3)  == "7") || // Bandera Santiago Del Estero                                                         
          (phoneNumber.charAt(3)  == "8")) { // Termas De Rio Hondo Santiago Del Estero                                                
            promptsArray = promptsArray.concat(
            promptBaseUrl + "miscellaneous/prefix" + audioFileExtension,
            phoneCharacteristicPrompts(phoneNumber.substring(0, 4)),                     
            promptBaseUrl + "miscellaneous/telefono" + audioFileExtension,                   
            phoneCharacteristicPrompts(phoneNumber.substring(4, 6)),                     
            promptBaseUrl + "miscellaneous/350ms" + audioFileExtension,                   
            phoneNumberPrompts(phoneNumber.substring(6)));          
        } else { // Santiago Del Estero Santiago Del Estero                                                 
            promptsArray = promptsArray.concat(
            promptBaseUrl + "miscellaneous/prefix" + audioFileExtension,
            phoneCharacteristicPrompts(phoneNumber.substring(0, 3)),                     
            promptBaseUrl + "miscellaneous/telefono" + audioFileExtension,                   
            phoneCharacteristicPrompts(phoneNumber.substring(3, 6)),                     
            promptBaseUrl + "miscellaneous/350ms" + audioFileExtension,                   
            phoneNumberPrompts(phoneNumber.substring(6)));
        }        

    } else if 
       ((phoneNumber.substring(0, 4)  == "3821") || // Nueva Esperanza Santiago Del Estero                                           
       (phoneNumber.substring(0, 4) == "3862") || // Trancas Tucuman                                                                               
       (phoneNumber.substring(0, 4) == "3863") || // Monteros Tucuman                                                                              
       (phoneNumber.substring(0, 4) == "3865") || // Concepcion (Prov. Tucuman) Tucuman                                                            
       (phoneNumber.substring(0, 4) == "3867") || // Tafi Del Valle Tucuman                                                                        
       (phoneNumber.substring(0, 4) == "3868") || // Cafayate Salta                                                                                
       (phoneNumber.substring(0, 4) == "3869")) { // Ranchillos Tucuman
       	  promptsArray = promptsArray.concat(
          promptBaseUrl + "miscellaneous/prefix" + audioFileExtension,
          phoneCharacteristicPrompts(phoneNumber.substring(0, 4)),                     
          promptBaseUrl + "miscellaneous/telefono" + audioFileExtension,                   
          phoneCharacteristicPrompts(phoneNumber.substring(4, 6)),                     
          promptBaseUrl + "miscellaneous/350ms" + audioFileExtension,                   
          phoneNumberPrompts(phoneNumber.substring(6)));                                                                            

    } else if (phoneNumber.substring(0, 3) == "387") {      
        if 
          ((phoneNumber.charAt(3) == "5") || // Tartagal (Prov. Salta) Salta                                        
          (phoneNumber.charAt(3)  == "6") || // Metan Salta                                                
          (phoneNumber.charAt(3)  == "7") || // Joaquin V. Gonzalez Salta                                                               
          (phoneNumber.charAt(3)  == "8")) { // Oran Salta                                                                                    
            promptsArray = promptsArray.concat(
            promptBaseUrl + "miscellaneous/prefix" + audioFileExtension,
            phoneCharacteristicPrompts(phoneNumber.substring(0, 4)),                     
            promptBaseUrl + "miscellaneous/telefono" + audioFileExtension,                   
            phoneCharacteristicPrompts(phoneNumber.substring(4, 6)),                     
            promptBaseUrl + "miscellaneous/350ms" + audioFileExtension,                   
            phoneNumberPrompts(phoneNumber.substring(6)));          
        } else { // Salta Salta                                           
            promptsArray = promptsArray.concat(
            promptBaseUrl + "miscellaneous/prefix" + audioFileExtension,
            phoneCharacteristicPrompts(phoneNumber.substring(0, 3)),                     
            promptBaseUrl + "miscellaneous/telefono" + audioFileExtension,                   
            phoneCharacteristicPrompts(phoneNumber.substring(3, 6)),                     
            promptBaseUrl + "miscellaneous/350ms" + audioFileExtension,                   
            phoneNumberPrompts(phoneNumber.substring(6)));
        }     

    } else if (phoneNumber.substring(0, 3) == "388") {      
        if 
          ((phoneNumber.charAt(3) == "4") || // San Pedro (Prov. Jujuy) Jujuy                                                               
          (phoneNumber.charAt(3)  == "5") || // La Quiaca Jujuy                                                                    
          (phoneNumber.charAt(3)  == "6") || // Libertador General San Martin Jujuy                                                                             
          (phoneNumber.charAt(3)  == "7")) { // Humahuaca Jujuy                                                                                                       
            promptsArray = promptsArray.concat(
            promptBaseUrl + "miscellaneous/prefix" + audioFileExtension,
            phoneCharacteristicPrompts(phoneNumber.substring(0, 4)),                     
            promptBaseUrl + "miscellaneous/telefono" + audioFileExtension,                   
            phoneCharacteristicPrompts(phoneNumber.substring(4, 6)),                     
            promptBaseUrl + "miscellaneous/350ms" + audioFileExtension,                   
            phoneNumberPrompts(phoneNumber.substring(6)));          
        } else { // San Salvador De Jujuy Jujuy                                               
            promptsArray = promptsArray.concat(
            promptBaseUrl + "miscellaneous/prefix" + audioFileExtension,
            phoneCharacteristicPrompts(phoneNumber.substring(0, 3)),                     
            promptBaseUrl + "miscellaneous/telefono" + audioFileExtension,                   
            phoneCharacteristicPrompts(phoneNumber.substring(3, 6)),                     
            promptBaseUrl + "miscellaneous/350ms" + audioFileExtension,                   
            phoneNumberPrompts(phoneNumber.substring(6)));
        }     

    } else if 
       ((phoneNumber.substring(0, 4)  == "3891") || // La Madrid Tucuman                                              
       (phoneNumber.substring(0, 4) == "3892") || // Amaicha Del Valle Tucuman                                                                                  
       (phoneNumber.substring(0, 4) == "3894")) { // Burruyacu Tucuman   
       	  promptsArray = promptsArray.concat(
          promptBaseUrl + "miscellaneous/prefix" + audioFileExtension,
          phoneCharacteristicPrompts(phoneNumber.substring(0, 4)),                     
          promptBaseUrl + "miscellaneous/telefono" + audioFileExtension,                   
          phoneCharacteristicPrompts(phoneNumber.substring(4, 6)),                     
          promptBaseUrl + "miscellaneous/350ms" + audioFileExtension,                   
          phoneNumberPrompts(phoneNumber.substring(6)));
       }      
       	                                                                          
/*
  } else if (phoneNumber.length == 7) {                                                 
      promptsArray = promptsArray.concat(                                               
      phoneThreeDigitGroupPrompts(phoneNumber.substring(0, 3)),                     
      promptBaseUrl + "miscellaneous/350ms" + audioFileExtension,                   
      alphanumericPrompts(phoneNumber.substring(3)));                               
  
*/
  
  } else {                                                                              
        promptsArray = alphanumericPrompts(phoneNumber);                                  
  }                                                                                     
  
  if (extension.length > 0) {                                                           
      promptsArray.push(promptBaseUrl + "miscellaneous/extension" + audioFileExtension);
      promptsArray = promptsArray.concat(alphanumericPrompts(extension));               
  }                                                                                     
  
    return promptsArray;                                                                  
}                                                                                         

function phoneThreeDigitGroupPrompts(value)                                               
{                                                                                         
  if (value.charAt(1) == "0" && value.charAt(2) == "0") {                               
    return cardinalPrompts(value);                                                    
  } else {                                                                              
      return alphanumericPrompts(value);                                                
  }                                                                                     
}   

function phoneCharacteristicPrompts(value)                                               
{                                                                                                                           
  return alphanumericPrompts(value);                                                    
}

function phoneNumberPrompts(value)                                               
{                                                                                                                           
  return cardinalPrompts(value);                                                    
}  


function timePrompts(value)
{
    var hours;
    var minutes;
    var seconds;
    var format;
    if (value.match(/^[0-9]+$/)) {
        if (value.length == 4) {
            hours = value.substring(0, 2);
            minutes = value.substring(2, 4);
        } else if (value.length == 6) {
            hours = value.substring(0, 2);
            minutes = value.substring(2, 4);
            seconds = value.substring(4, 6);
        } else {
            return void 0;
        }
        if (hours >= 1 && hours <= 12) {
            format = "?";
        } else if (hours == 0 || (hours >= 13 && hours <= 24)) {
            format = "h";
        } else {
            return void 0;
        }
    } else if (value.match(/^[0-9]+[?hap]$/)) {
        if (value.length == 5) {
            hours = value.substring(0, 2);
            minutes = value.substring(2, 4);
            format = value.substring(4, 5);
        } else if (value.length == 7) {
            hours = value.substring(0, 2);
            minutes = value.substring(2, 4);
            seconds = value.substring(4, 6);
            format = value.substring(6, 7);
        } else {
            return void 0;
        }
    } else {
        return void 0;
    }

    var promptsArray = new Array();
    promptsArray = promptsArray.concat(cardinalPrompts(hours));
    if (minutes == "00" && (format == "a" || format == "p" || format == "?")) {
      promptsArray.push(promptBaseUrl + "miscellaneous/oclock" + audioFileExtension);
    } else {
        promptsArray.push(promptBaseUrl + "miscellaneous/hours" + audioFileExtension);
    }
    if (minutes > 0 && minutes < 10) {
        promptsArray = promptsArray.concat(alphanumericPrompts("0"));
        promptsArray = promptsArray.concat(cardinalPrompts(minutes));
        promptsArray.push(promptBaseUrl + "miscellaneous/minutes" + audioFileExtension);
    } else {
        promptsArray = promptsArray.concat(cardinalPrompts(minutes));
        promptsArray.push(promptBaseUrl + "miscellaneous/minutes" + audioFileExtension);
    }



    if (seconds != undefined && seconds != "00")  {
        promptsArray.push(promptBaseUrl + "miscellaneous/and" + audioFileExtension);
        promptsArray.push(cardinalPrompts(seconds));
        if (seconds == 1) {
            promptsArray.push(promptBaseUrl + "miscellaneous/second" + audioFileExtension);
        } else {
            promptsArray.push(promptBaseUrl + "miscellaneous/seconds" + audioFileExtension);
        }
    }
    
    if (format == "a") {
        promptsArray.push(promptBaseUrl + "miscellaneous/am" + audioFileExtension);
    } else if (format == "p") {
        promptsArray.push(promptBaseUrl + "miscellaneous/pm" + audioFileExtension);
    }
    
    return promptsArray;
}

function yearPrompts(year)
{
    var century = year.substring(0,2);
    var rest = year.substring(2,4);
    var rest1900 = year.substring(1,4); //centenas del 1900
    var promptsArray = new Array();
    if (century == "20") {
        promptsArray = promptsArray.concat(cardinalPrompts(2000));
    } else {
    	promptsArray = promptsArray.concat(cardinalPrompts(1000)); //pronuncio el año como mil 
    }
    if (century != "20") {
        if (rest1900 != "000") {
        	promptsArray = promptsArray.concat(cardinalPrompts(rest1900));
        }
    } else {
        if (rest != "00") {
            promptsArray = promptsArray.concat(cardinalPrompts(rest))
        }
    }
    return promptsArray;
}

function monthPrompts(month)
{
    if (month >= 1 && month <= 12) {
        return new Array(promptBaseUrl + "months/" + month + audioFileExtension);
    } else {
        return void 0;
    }
}

function dayOfWeekPrompts(dayOfWeek)
{
    if (dayOfWeek >= 0 && dayOfWeek <= 6) {
        return new Array(promptBaseUrl + "days/" + dayOfWeek + audioFileExtension);
    } else {
        return void 0;
    }
}
function cardinalPrompts(number)
{
    if (number === undefined || !isFinite(number)) {
        return void 0;
    }
    if (number > 999999999999999) {
        return void 0;
    }
    var isNegative;
    if (number < 0) {
        isNegative = true;
        number = Math.abs(number);
    }
    
    if (number == 0) {
        return new Array(promptBaseUrl + "cardinals/000" + audioFileExtension);
    }
    var str = new String(number);
    var arr = str.split(".");
    number  = new Number(arr[0]);
    if (arr[1] != undefined)
    {
    	var fractionalPart = new Number(arr[1]);
    }
    
    /*var fractionalPart = number - Math.floor(number);
    number = number - fractionalPart;*/
    var promptsArray = new Array();
    var magnitude = 0;
    while (number > 0) {
        var endDigits = number % 1000;
        if (endDigits != 0) {
            if ( number == 1 && magnitude==1 ) /* mil algo */
            {	
              promptsArray = magnitudePrompts(magnitude).concat(promptsArray);
            }
            else
            { 
              if ( number > 1 && magnitude==2 ){	
                promptsArray = threeDigitsPrompts(endDigits).concat(magnitudePrompts(5), promptsArray);
              }
              else {
            	promptsArray = threeDigitsPrompts(endDigits).concat(magnitudePrompts(magnitude), promptsArray);  
              }
            }
        }
        number = number - endDigits;
        number = number / 1000;
        magnitude++;
    }
    if (isNegative) {
        promptsArray.unshift(promptBaseUrl + "miscellaneous/minus" + audioFileExtension);
    }

    if ( (fractionalPart != undefined) && (fractionalPart != 0) ) {
        fractionalPart = fractionalPart.toString();//.substring(2);
        promptsArray.push(promptBaseUrl + "miscellaneous/point" + audioFileExtension); 
        promptsArray = promptsArray.concat(alphanumericPrompts(fractionalPart));
    }
    return promptsArray;
} 

function threeDigitsPrompts(number)  
{  
  // assert number >=0 && number <= 999  
  if (number < 0 || number > 999) {  
    return void 0;  
  }  
  
  var hundreds = Math.floor(number / 100);  
  var tensAndOnes = number % 100;  
  var promptsArray = new Array(); 
 
  if (hundreds > 0) { 
    if (hundreds < 2) { 
      if (tensAndOnes > 0) { 
        promptsArray.push(promptBaseUrl + "cardinals/ciento" + audioFileExtension); 
      } else { 
          promptsArray.push(promptBaseUrl + "cardinals/" + hundreds + "00" + audioFileExtension); 
      } 
    } else { 
        promptsArray.push(promptBaseUrl + "cardinals/" + hundreds + "00" + audioFileExtension); 
    } 
  } 
 
  if (tensAndOnes > 0) {  
    if (tensAndOnes < 10) { // chequeo numero de de 1 a 9 
      if (tensAndOnes < 2) { // numero es 1
        promptsArray.push(promptBaseUrl + "cardinals/un" + audioFileExtension); 
      } else { 
          promptsArray.push(promptBaseUrl + "cardinals/00" + tensAndOnes + audioFileExtension); // numero de 2 a 9
      } 
    } else {
        switch (tensAndOnes) {
          case 21: 
          case 31: 
          case 41: 
          case 51: 
          case 61: 
          case 71: 
          case 81:
          case 91:
            promptsArray.push(promptBaseUrl + "cardinals/0" + tensAndOnes + "un" + audioFileExtension);
            break;
          default:
            promptsArray.push(promptBaseUrl + "cardinals/0" + tensAndOnes + audioFileExtension);  
        }              
    }  
  } 
  return promptsArray;  
}      

function ordinalThreeDigitsPrompts(number)
{
    // assert number >=0 && number <= 999
    if (number < 0 || number > 999) {
        return void 0;
    }

    var hundreds = Math.floor(number / 100);
    var tensAndOnes = number % 100;

    var promptsArray = new Array();
    if (hundreds > 0) {
      if (tensAndOnes > 0) {
            promptsArray.push(promptBaseUrl + "cardinals/" + hundreds + "00" + audioFileExtension);
      } else {
            promptsArray.push(promptBaseUrl + "cardinals/00" + hundreds + audioFileExtension);
            promptsArray.push(promptBaseUrl + "ordinals/hundredth" + audioFileExtension);
      }
    }

    if (tensAndOnes > 0) {
        if (tensAndOnes < 10) {
            promptsArray.push(promptBaseUrl + "ordinals/00" + tensAndOnes + audioFileExtension);
        } else {
            promptsArray.push(promptBaseUrl + "ordinals/0" + tensAndOnes + audioFileExtension);
        }
    }
    return promptsArray;
}

/**
 * Returns an array of vox file URLs corresponding to the
 * number 10^(3*number).
 *
 * Note the differences between the American and European systems:
 * <pre>
 *       Order of
 *       Magnitude      American    European
 *         10^0            -           -
 *         10^3         thousand    thousand
 *         10^6          million     million
 *         10^9          billion    thousand million or milliard
 *         10^12        trillion     billion
 * </pre>
 *
 * @param number The order of magnitude.
 * @return An array of URL strings.
 */
function magnitudePrompts(number)
{
    switch (number) {
        case 0:
            return new Array();
        case 1:
            return new Array(promptBaseUrl + "cardinals/thousand" + audioFileExtension); 
        case 2:
            return new Array(promptBaseUrl + "cardinals/million" + audioFileExtension);
        case 3:
            return new Array(promptBaseUrl + "cardinals/billion" + audioFileExtension);
        case 4:
            return new Array(promptBaseUrl + "cardinals/trillion" + audioFileExtension);
        case 5:
            return new Array(promptBaseUrl + "cardinals/millions" + audioFileExtension);    
        default:
            return void 0;
    }
}

function ordinalMagnitudePrompts(number)
{
    switch (number) {
        case 0:
            return new Array();
        case 1:
            return new Array(promptBaseUrl + "ordinals/thousandth" + audioFileExtension); 
        case 2:
            return new Array(promptBaseUrl + "ordinals/millionth" + audioFileExtension); 
        case 3:
            return new Array(promptBaseUrl + "ordinals/billionth" + audioFileExtension); 
        case 4:
            return new Array(promptBaseUrl + "ordinals/trillionth" + audioFileExtension); 
        default:
            return void 0;
    }
}

function ordinalPrompts(number)
{
    if (number === undefined || !isFinite(number) || number <= 0 ||
        ((number - Math.floor(number)) != 0) || number > 999999999999999 ) {
        return void 0;
    }

    var promptsArray = new Array();
    var magnitude = 0;
    var ordinal = true;
    while (number > 0) {
        var endDigits = number % 1000;
        if (endDigits != 0) {
          if (magnitude == 0 && ordinal) {
            promptsArray =
            ordinalThreeDigitsPrompts(endDigits).concat(promptsArray);
            ordinal = false;
          } else {
              if (ordinal) {
                promptsArray = threeDigitsPrompts(endDigits).concat(ordinalMagnitudePrompts(magnitude), promptsArray);
                ordinal = false;
              } else {
                  promptsArray = threeDigitsPrompts(endDigits).concat(magnitudePrompts(magnitude), promptsArray);
              }
          }
        }
        number = number - endDigits;
        number = number / 1000;
        magnitude++;
    }
    return promptsArray;
}

function alphanumericPrompts(string)
{
    var i;
    var ch;
    var promptsArray = new Array();
    string = string.toLowerCase();
    for (i = 0; i < string.length; i++) {
        ch = string.charAt(i);
        ch = ch.toLowerCase();
        if (ch.match(/\d/)) {
            promptsArray.push(promptBaseUrl + "cardinals/00" + ch + audioFileExtension);
        } else if (ch.match(/[a-z]/)) {
            promptsArray.push(promptBaseUrl + "letters/" + ch + audioFileExtension);
        } else if (ch.match(/\+/)) {
            promptsArray.push(promptBaseUrl + "miscellaneous/plus" + audioFileExtension);
        } else if (ch.match(/\</)) {
            promptsArray.push(promptBaseUrl + "miscellaneous/lessthan" + audioFileExtension);
        } else if (ch.match(/\=/)) {
            promptsArray.push(promptBaseUrl + "miscellaneous/equals" + audioFileExtension);
        } else if (ch.match(/\%/)) {
            promptsArray.push(promptBaseUrl + "miscellaneous/percent" + audioFileExtension);
        } else if (ch.match(/\-/)) {
            promptsArray.push(promptBaseUrl + "miscellaneous/minus" + audioFileExtension);
        } else if (ch.match(/\>/)) {
            promptsArray.push(promptBaseUrl + "miscellaneous/greaterthan" + audioFileExtension);
        } else if (ch.match(/\&/)) {
            promptsArray.push(promptBaseUrl + "miscellaneous/and" + audioFileExtension);
        } else if (ch.match(/\./)) {
            promptsArray.push(promptBaseUrl + "miscellaneous/dot" + audioFileExtension);
        } else if (ch.match(/\#/)) {
            promptsArray.push(promptBaseUrl + "miscellaneous/pound" + audioFileExtension);
        } else if (ch.match(/\*/)) {
            promptsArray.push(promptBaseUrl + "miscellaneous/star" + audioFileExtension);
        } else if (ch.match(/\@/)) {
            promptsArray.push(promptBaseUrl + "miscellaneous/at" + audioFileExtension);
        }
    }
    return promptsArray;
}

function dtmfPrompts(string)
{
    var i;
    var ch;
    var promptsArray = new Array();
    string = string.toLowerCase();

    for (i = 0; i < string.length; i++) {
        ch = string.charAt(i);
        if (ch.match(/[0-9abcd]/)) {
            promptsArray.push(promptBaseUrl + "dtmf/" + ch + audioFileExtension);
        } else if (ch == "#") {
            promptsArray.push(promptBaseUrl + "dtmf/pound" + audioFileExtension);
        } else if (ch == "*") {
            promptsArray.push(promptBaseUrl + "dtmf/star" + audioFileExtension);
        }
    }
    return promptsArray;
}
